package edu.parallel.baiev.private3;

import java.util.concurrent.Exchanger;

public class App {
    public static void main(String[] args) {
        long l = System.currentTimeMillis();
        int length = 4096;
        int[] vector = new int[10];
        for (int i = 0; i < 10; ++i) {
            vector[i] = i + 1;
        }
        int[][] matrix = new int[length][10];
        int k = 10 + 1;
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[i].length; ++j) {
                matrix[i][j] = k++;
            }
        }


        Exchanger<DTO> firstSecondExchanger = new Exchanger<>();
        Exchanger<DTO> secondThirdExchanger = new Exchanger<>();
        Exchanger<DTO> thirdFourthExchanger = new Exchanger<>();

        new CommonThread(firstSecondExchanger).start();
        new SecondThread(firstSecondExchanger, secondThirdExchanger, vector).start();
        new ThirdThread(secondThirdExchanger, thirdFourthExchanger, matrix).start();
        new CommonThread(thirdFourthExchanger).start();
    }
}
