package edu.parallel.baiev.private3;

public interface SolutionCalculator {

    default int calculate(int[] vector, int[] matrixRow) {
        int result = 0;
        for (int i = 0; i < vector.length; ++i) {
            result += vector[i] * matrixRow[i];
        }
        return result;
    }
}
