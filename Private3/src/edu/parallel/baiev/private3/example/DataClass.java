package edu.parallel.baiev.private3.example;

public class DataClass {
    int[][] value;
    private int currentRow;

    DataClass(int r, int c) {
        value = new int[r][c];
        currentRow = 0;
    }

    public void addVector(int[] vector) {
        for (int i = 0; i < value[currentRow].length; ++i) {
            value[currentRow][i] = vector[i];
        }
        ++currentRow;
    }

    public int[][] getValue() {
        return value;
    }
}
