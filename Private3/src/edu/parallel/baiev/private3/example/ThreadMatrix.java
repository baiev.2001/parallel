package edu.parallel.baiev.private3.example;

import java.util.concurrent.Exchanger;

public class ThreadMatrix implements Runnable {
    int pid;
    int[][] matrix;
    Exchanger<DataClass> exchanger23;
    Exchanger<DataClass> exchanger34;

    ThreadMatrix(int id, int rows, int cols, Exchanger<DataClass> ex23, Exchanger<DataClass> ex34) {
        pid = id;
        exchanger23 = ex23;
        exchanger34 = ex34;
        matrix = new int[rows][cols];
        int k = cols + 1;
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[i].length; ++j) {
                matrix[i][j] = k++;
            }
        }
    }

    public void run() {
        try {
            DataClass data = new DataClass(matrix.length / 2, matrix[0].length);
            for (int i = 0; i < matrix.length / 2; ++i) {
                data.addVector(matrix[i]);
            }
            data = exchanger23.exchange(data);
            int[][] vector = data.getValue();
            data = exchanger34.exchange(data);
            data = new DataClass(matrix.length / 4, matrix[0].length);
            for (int i = matrix.length * 3 / 4; i < matrix.length; ++i) {
                data.addVector(matrix[i]);
            }
            data = exchanger34.exchange(data);
            int[] result = new int[matrix.length / 2];
            int k = 0;
            for (int i = matrix.length / 2; i < matrix.length * 3 / 4; ++i) {
                int m = 0;
                for (int j = 0; j < matrix[i].length; ++j) {
                    m += vector[0][j] * matrix[i][j];
                }
                result[k++] = m;
                System.out.println("TMatrix(" + pid + "):run:result[" + i + "] = " + m);
            }
            data = exchanger34.exchange(data);
            int[][] v4 = data.getValue();
            for (int i = 0; i < v4[0].length; ++i) {
                result[k++] = v4[0][i];
            }
            data = new DataClass(1, result.length);
            data.addVector(result);
            data = exchanger23.exchange(data);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
