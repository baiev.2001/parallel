package edu.parallel.baiev.private3.example;

import java.util.concurrent.Exchanger;

public class ThreadCommon implements Runnable {
    int pid;
    Exchanger<DataClass> exchanger;

    ThreadCommon(int id, Exchanger<DataClass> ex) {
        pid = id;
        exchanger = ex;
    }

    public void run() {
        try {
            DataClass vData = new DataClass(1, 10);
            vData = exchanger.exchange(vData);
            int[][] v = vData.getValue();

            DataClass mData = new DataClass(3, 3);
            mData = exchanger.exchange(mData);
            int[][] m = mData.getValue();
            int[] result = new int[m.length];

            for (int i = 0; i < m.length; ++i) {
                result[i] = 0;
                for (int j = 0; j < m[i].length; ++j) {
                    result[i] += v[0][j] * m[i][j];
                }
                System.out.println("TCommon(" + pid + "):run:result[" + i + "] = " + result[i]);
            }
            DataClass rData = new DataClass(1, result.length);
            rData.addVector(result);
            rData = exchanger.exchange(rData);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
