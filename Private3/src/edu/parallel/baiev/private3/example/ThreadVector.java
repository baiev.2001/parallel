package edu.parallel.baiev.private3.example;

import java.util.concurrent.Exchanger;

public class ThreadVector implements Runnable {
    int pid;
    int[] vector;
    Exchanger<DataClass> exchangerFirstTwo;
    Exchanger<DataClass> exchangerTwoThree;

    ThreadVector(int id, int vlen, Exchanger<DataClass> exchangerFirstTwo, Exchanger<DataClass> exchangerTwoThree) {
        this.pid = id;
        this.exchangerFirstTwo = exchangerFirstTwo;
        this.exchangerTwoThree = exchangerTwoThree;
        this.vector = new int[vlen];
        for (int i = 0; i < vector.length; ++i) {
            this.vector[i] = i + 1;
        }
    }

    public void run() {
        try {
            long l = System.currentTimeMillis();
            DataClass vectorData = new DataClass(1, vector.length);
            vectorData.addVector(vector);
            DataClass matrixData = exchangerTwoThree.exchange(vectorData);
            int[][] matrix = matrixData.getValue();
            DataClass data = exchangerFirstTwo.exchange(vectorData);
            data = new DataClass(matrix.length / 2, matrix[0].length);
            for (int i = 0; i < matrix.length / 2; ++i) {
                data.addVector(matrix[i]);
            }

            data = exchangerFirstTwo.exchange(data);

            int[] result = new int[matrix.length];
            for (int i = matrix.length / 2; i < matrix.length; ++i) {
                result[i] = 0;
                for (int j = 0; j < matrix[i].length; ++j) {
                    result[i] += vector[j] * matrix[i][j];
                }
                System.out.println("TVector(" + pid + "):run:result[" + i + "] = " + result[i]);
            }

            int[] max = new int[3];
            for (int i = 0; i < max.length; ++i) {
                max[i] = 0;
            }
            data = exchangerFirstTwo.exchange(data);

            int[][] v1 = data.getValue();
            for (int i = 0; i < v1[0].length; ++i) {
                System.out.println("TVector(" + pid + "):run:t1:result[" + i + "] = " + v1[0][i]);
                if (max[0] < v1[0][i]) {
                    max[0] = v1[0][i];
                }
            }
            System.out.println("TVector(" + pid + "):Max from T1 = " + max[0]);

            for (int i = matrix.length / 2; i < matrix.length; ++i) {
                System.out.println("TVector(" + pid + "):run:t2:result[" + i + "] = " + result[i]);
                if (max[1] < result[i]) {
                    max[1] = result[i];
                }
            }
            System.out.println("TVector(" + pid + "):Max TVector = " + max[1]);

            data = exchangerTwoThree.exchange(data);
            int[][] v2 = data.getValue();
            for (int i = 0; i < v2[0].length; ++i) {
                System.out.println("TVector(" + pid + "):run:t3:result[" + i + "] = " + v2[0][i]);
                if (max[2] < v2[0][i]) {
                    max[2] = v2[0][i];
                }
            }
            System.out.println("TVector(" + pid + "):Max from T3 & T4 = " + max[2]);

            for (int i = 0; i < max.length; ++i) {
                if (max[0] < max[i]) {
                    max[0] = max[i];
                }
            }
            System.out.println("TVector(" + pid + "):MaxResult = " + max[0]);

            System.out.println("Time: " + (System.currentTimeMillis() - l));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
