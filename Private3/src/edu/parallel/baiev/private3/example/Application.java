package edu.parallel.baiev.private3.example;

import java.util.concurrent.Exchanger;

public class Application {
    public static void main(String[] args) {
        System.out.println("Main process started");
        int cols = 8;
        int rows = 16;
        Exchanger<DataClass> exchangerFirstTwo = new Exchanger<>();
        Exchanger<DataClass> exchangerTwoThree = new Exchanger<>();
        Exchanger<DataClass> exchangerThreeFour = new Exchanger<>();
        (new Thread(new ThreadCommon(1, exchangerFirstTwo))).start();
        (new Thread(new ThreadVector(2, cols, exchangerFirstTwo, exchangerTwoThree))).start();
        (new Thread(new ThreadMatrix(3, rows, cols, exchangerTwoThree, exchangerThreeFour))).start();
        (new Thread(new ThreadCommon(4, exchangerThreeFour))).start();
        System.out.println("Main process ended");
    }
}
