package edu.parallel.baiev.private3;

import java.util.concurrent.Exchanger;

public class ThirdThread extends Thread implements SolutionCalculator {
    Exchanger<DTO> secondThirdExchange;
    Exchanger<DTO> thirdFourthExchange;

    private int[][] matrix;


    public ThirdThread(Exchanger<DTO> secondThirdExchange, Exchanger<DTO> thirdFourthExchange, int[][] matrix) {
        this.secondThirdExchange = secondThirdExchange;
        this.thirdFourthExchange = thirdFourthExchange;
        this.matrix = matrix;
    }

    @Override
    public void run() {
        try {
            DTO halfMatrixDTO = new DTO(matrix.length / 2, matrix[0].length);
            for (int i = 0; i < matrix.length / 2; ++i) {
                halfMatrixDTO.saveVector(matrix[i]);
            }
            DTO vectorDto = secondThirdExchange.exchange(halfMatrixDTO);
            thirdFourthExchange.exchange(vectorDto);

            int thirdQuarterId = 3 * (matrix.length / 4);
            DTO lastQuarterMatrixDTO = new DTO(matrix.length - thirdQuarterId, matrix[0].length);
            for (int i = thirdQuarterId; i < matrix.length; ++i) {
                lastQuarterMatrixDTO.saveVector(matrix[i]);
            }
            thirdFourthExchange.exchange(lastQuarterMatrixDTO);

            int[] result = new int[matrix.length / 4];
            int j = 0;
            for (int i = matrix.length / 2; i < thirdQuarterId; ++i) {
                result[j++] = calculate(vectorDto.getPayload()[0], matrix[i]);
            }

            DTO fourthResultDTO = thirdFourthExchange.exchange(null);

            DTO resultDTO = new DTO(2, fourthResultDTO.getPayload()[0].length);
            resultDTO.saveVector(result);
            resultDTO.saveVector(fourthResultDTO.getPayload()[0]);
            secondThirdExchange.exchange(resultDTO);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}
