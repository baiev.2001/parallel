package edu.parallel.baiev.private3;

import java.util.concurrent.Exchanger;

public class SecondThread extends Thread implements SolutionCalculator {

    private Exchanger<DTO> firstSecondExchange;
    private Exchanger<DTO> secondThirdExchange;

    private int[] vector;

    public SecondThread(Exchanger<DTO> firstSecondExchange, Exchanger<DTO> secondThirdExchange, int[] vector) {
        this.firstSecondExchange = firstSecondExchange;
        this.secondThirdExchange = secondThirdExchange;
        this.vector = vector;
    }

    @Override
    public void run() {
        try {
            long l = System.currentTimeMillis();

            DTO vectorDto = DTO.ofVector(vector);
            firstSecondExchange.exchange(vectorDto);

            DTO matrixDto = secondThirdExchange.exchange(vectorDto);
            int[][] matrix = matrixDto.getPayload();

            DTO firstProcessMatrix = new DTO(matrix.length / 2, matrix[0].length);
            for (int i = 0; i < matrix.length / 2; ++i) {
                firstProcessMatrix.saveVector(matrix[i]);
            }
            firstSecondExchange.exchange(firstProcessMatrix);

            int[] secondProcessResult = new int[matrix.length];

            int k = 0;
            for (int i = matrix.length / 2; i < matrix.length; ++i) {
                secondProcessResult[k++] = calculate(vector, matrix[i]);
            }

            int[] firstProcessResult = firstSecondExchange.exchange(null).getPayload()[0];
            int[][] thirdFourthResults = secondThirdExchange.exchange(null).getPayload();

            int[] resultArray = new int[matrix.length * 2 + 1];
            int n = 0;
            n = mergeArray(firstProcessResult, n, resultArray);
            n = mergeArray(secondProcessResult, n, resultArray);
            n = mergeArray(thirdFourthResults[0], n, resultArray);
            mergeArray(thirdFourthResults[1], n, resultArray);
            for (Integer number : resultArray) {
                System.out.print(number + "  ");
            }

            System.out.println("\nTime: " + (System.currentTimeMillis() - l));
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    private int mergeArray(int[] firstProcessResult, int n, int[] resultArray) {
        for (int i = 0; i < firstProcessResult.length; ++i) {
            if (firstProcessResult[i] == 0) {
                continue;
            }
            resultArray[n++] = firstProcessResult[i];
        }
        return n;
    }


}
