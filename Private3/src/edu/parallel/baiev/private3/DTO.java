package edu.parallel.baiev.private3;

public class DTO {
    private int[][] payload;
    private int currentId;

    public DTO(int rows, int columns) {
        this.payload = new int[rows][columns];
    }

    public DTO(int[][] payload) {
        this.payload = payload;
    }


    public static DTO ofVector(int[] array) {
        DTO dto = new DTO(1, array.length);
        for (int i = 0; i < array.length; ++i) {
            dto.payload[0][i] = array[i];
        }
        return dto;
    }

    public int[][] getPayload() {
        return payload;
    }

    public void setArrayId(int currentId) {
        this.currentId = currentId;
    }

    public void saveVector(int[] array) {
        for (int i = 0; i < array.length; ++i) {
            payload[currentId][i] = array[i];
        }
        ++currentId;
    }
}
