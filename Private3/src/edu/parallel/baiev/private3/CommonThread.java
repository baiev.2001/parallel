package edu.parallel.baiev.private3;

import java.util.concurrent.Exchanger;

public class CommonThread extends Thread implements SolutionCalculator {
    Exchanger<DTO> exchanger;

    public CommonThread(Exchanger<DTO> exchanger) {
        this.exchanger = exchanger;
    }

    @Override
    public void run() {
        try {
            DTO vectorDto = exchanger.exchange(null);
            DTO matrixDto = exchanger.exchange(null);

            int[] vector = vectorDto.getPayload()[0];
            int[][] matrix = matrixDto.getPayload();

            int[] result = new int[matrix.length];
            int j = 0;
            for (int i = 0; i < matrix.length; ++i) {
                result[j++] = calculate(vector, matrix[i]);
            }
            exchanger.exchange(DTO.ofVector(result));
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}
