package edu.parallel.baiev.private1;


public class MatrixMonitor extends Thread {
    private final int[][] result;
    private final int[][] matrixB;
    private final int[][] matrixC;
    private int procNum = 0;
    private long time;

    MatrixMonitor(int[][] result, int[][] matrixB, int[][] matrixC, long time) {
        this.result = result;
        this.matrixB = matrixB;
        this.matrixC = matrixC;
        this.time = time;
    }

    MatrixMonitor(int[][] result, int[][] matrixB, int[][] matrixC) {
        this.result = result;
        this.matrixB = matrixB;
        this.matrixC = matrixC;
        this.time = System.currentTimeMillis();
    }

    @Override
    public synchronized void start() {
        for (int i = 0; i < result.length; ++i) {
            (new MatrixCalcThread(i + 1,
                    result,
                    matrixB,
                    matrixC,
                    i,
                    i + 1,
                    this)).start();
            procIncrement();
        }
        super.start();
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                while (getProcNum() > 0) {
                    wait();
                }
            }
        } catch (InterruptedException exception) {
            System.out.println("Interrupted exception " + exception);
        }
        for (int i = 0; i < result.length; ++i) {
            for (int j = 0; j < result[i].length; ++j) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println(" ");
        }
        System.out.println("Time: " + (System.nanoTime() - this.time));
    }

    public synchronized void procIncrement() {
        ++procNum;
    }

    public synchronized void procDecrement() {
        --procNum;
        notify();
    }

    public synchronized int getProcNum() {
        return procNum;
    }
}
