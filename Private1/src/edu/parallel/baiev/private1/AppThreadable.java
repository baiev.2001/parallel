package edu.parallel.baiev.private1;

public class AppThreadable {

    public static void main(String[] args) {
        long l = System.nanoTime();
        int[][] vector = new int[][]{
                {1},
                {2},
                {3},
                {4},
                {5}
        };
        int[][] matrix = new int[][]{
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25}
        };
        int[][] result = new int[matrix.length][vector[0].length];

        MatrixMonitor matrixMonitor = new MatrixMonitor(result, matrix, vector, l);
        matrixMonitor.start();
    }
}
//2301700
//277400