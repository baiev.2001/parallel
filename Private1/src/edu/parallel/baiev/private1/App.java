package edu.parallel.baiev.private1;

public class App {

    public static void main(String[] args) {
        int mSize = 4;

        int ma[][] = new int[mSize][mSize];
        int mb[][] = new int[][]{
                {1, 2, 3, 4}, {5, 6, 7, 8}, {1, 0, 1, 0}, {0, 0, 0, 1}
        };
        int mc[][] = new int[][]{
                {8, 5, 2, 4}, {1, 4, 3, 8}, {1, 1, 1, 1}, {0, 0, 0, 0}
        };
        for (int i = 0; i < mSize; ++i) {
            for (int j = 0; j < mSize; ++j) {
                ma[i][j] = 0;
//                mc[i][j] = 1;
            }
        }
        (new MatrixMonitor(ma, mb, mc)).start();
    }
}
