package edu.parallel.baiev.private1;

/**
 *
 */
public class MatrixCalcThread extends Thread {
    private final int[][] result;
    private final int[][] matrixB;
    private final int[][] matrixC;
    private final int lowerBound;
    private final int higherBound;
    private final int threadNumber;
    private final MatrixMonitor mainThread;

    public MatrixCalcThread(int threadNumber,
                            int[][] resultPointer,
                            int[][] matrixB,
                            int[][] matrixC,
                            int lowerBound,
                            int higherBound,
                            MatrixMonitor monitor) {
        this.result = resultPointer;
        this.matrixB = matrixB;
        this.matrixC = matrixC;
        this.lowerBound = lowerBound;
        this.higherBound = higherBound;
        this.threadNumber = threadNumber;
        this.mainThread = monitor;
    }

    @Override
    public void run() {
        try {
            for (int i = lowerBound; i < higherBound; ++i) {
                for (int j = 0; j < matrixC[i].length; ++j) {
                    result[i][j] = 0;
                    if (matrixB[j].length != matrixC.length) {
                        throw new RuntimeException(String.format("[Thread-%d] Wrong number of columns of matrixB. Expected: %d. Actual %d", threadNumber, matrixC.length, matrixB[j].length));
                    }
                    for (int k = 0; k < matrixB[j].length; ++k) {
                        result[i][j] += matrixB[i][k] * matrixC[k][j];
//                    Uncomment it to Debug.
//                    System.out.printf("[Thread-%d]: result += %d * %d. Equals: %d\n", threadNumber, matrixB[j][k], matrixC[i][j], result[i][j]);
                    }
                }
            }
        } finally {
            mainThread.procDecrement();
        }
    }
}
