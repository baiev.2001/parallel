package edu.parallel.baiev.private1.example;

public class App {

    public static void main(String[] args) {
        int mSize = 4;

        int ma[][] = new int[mSize][mSize];
        int mb[][] = new int[mSize][mSize];

        int mc[][] = new int[mSize][mSize];

        for (int i = 0; i < mSize; ++i) {
            for (int j = 0; j < mSize; ++j) {
                ma[i][j] = 0;
//                mb[i][j] = mc[i][j] = 1;
            }
        }
        (new MatrixMonitor(ma, mb, mc)).start();
    }
}
