package edu.parallel.baiev.private1.example;

public class MatrixCalcThread extends Thread {
    private final int ma[][];
    private final int mb[][];
    private final int mc[][];
    private final int lo, hi, threadNumber;
    private final MatrixMonitor mainThread;

    public MatrixCalcThread(int thNum, int a[][], int b[][], int c[][], int l, int h, MatrixMonitor mt) {
        ma = a;
        mb = b;
        mc = c;
        lo = l;
        hi = h;
        threadNumber = thNum;
        mainThread = mt;
    }

    @Override
    public void run() {
        for (int i = lo; i < hi; ++i) {
            for (int j = 0; j < mc[i].length; ++j) {
                ma[i][j] = 0;
                for (int k = 0; k < mb[j].length; ++k) {
                    ma[i][j] += mb[j][k] * mc[i][j];
                }
            }
        }
        mainThread.procDecrement();
    }
}
