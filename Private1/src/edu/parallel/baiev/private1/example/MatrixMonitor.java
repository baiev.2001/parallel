package edu.parallel.baiev.private1.example;

public class MatrixMonitor extends Thread {
    int ma[][];
    int mb[][];
    int mc[][];
    private int procNum = 0;

    MatrixMonitor(int a[][], int b[][], int c[][]) {
        ma = a;
        mb = b;
        mc = c;
    }

    @Override
    public synchronized void start() {
        for (int i = 0; i < ma.length; ++i) {
            (new MatrixCalcThread(i + 1, ma, mb, mc, i, i + 1, this)).start();
            procIncrement();
        }
        super.start();
    }

    @Override
    public void run() {
        try {
            synchronized (this) {
                while (getProcNum() > 0) {
                    wait();
                }
            }
        } catch (InterruptedException exception) {
            System.out.println("Interrupted exception " + exception);
        }
        for (int i = 0; i < ma.length; ++i) {
            for (int j = 0; j < ma.length; ++j) {
                System.out.print(ma[i][j] + " ");
            }
            System.out.println(" ");
        }
    }

    public synchronized void procIncrement() {
        ++procNum;
    }

    public synchronized void procDecrement() {
        --procNum;
        notify();
    }

    public synchronized int getProcNum() {
        return procNum;
    }
}
