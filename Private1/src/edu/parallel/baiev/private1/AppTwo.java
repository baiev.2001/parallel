package edu.parallel.baiev.private1;

public class AppTwo {

    public static void main(String[] args) {
        long l = System.nanoTime();
        int[][] vector = new int[][]{
                {1},
                {2},
                {3},
                {4},
                {5}
        };
        int[][] matrix = new int[][]{
                {6, 7, 8, 9, 10},
                {11, 12, 13, 14, 15},
                {16, 17, 18, 19, 20},
                {21, 22, 23, 24, 25}
        };
        int[][] result = new int[matrix.length][vector[0].length];

        for (int i = 0; i < matrix.length; ++i) {
            if (matrix[i].length != vector.length) {
                throw new RuntimeException("Matrix has wrong number of columns");
            }
            int a = 0;
            for (int j = 0; j < matrix[i].length; ++j) {
                a += matrix[i][j] * vector[j][0];
            }
            result[i][0] = a;
        }
        for (int i = 0; i < result.length; ++i) {
            for (int j = 0; j < result[i].length; ++j) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println(" ");
        }
        System.out.println("Time: " + (System.nanoTime() - l));
    }

    static void printMatrix(int[][] arr) {
        for (int i = 0; i < arr.length; ++i) {
            for (int j = 0; j < arr[i].length; ++j) {
                System.out.printf("%d\t", arr[i][j]);
            }
            System.out.println();
        }
    }
}
