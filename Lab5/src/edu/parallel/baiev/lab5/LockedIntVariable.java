package edu.parallel.baiev.lab5;

public class LockedIntVariable {
    private final Lock lock;
    private Integer value;

    public LockedIntVariable(Integer startValue) {
        this.lock = new Lock();
        this.value = startValue;
    }

    public Integer inc() {
        try {
            this.lock.lock();
            ++this.value;
            this.lock.unlock();
        } catch (InterruptedException exception) {
            System.out.println("Exception occured!");
            exception.printStackTrace();
        }
        return this.value;
    }

    public synchronized Integer getValue() {
        return this.value;
    }
}
