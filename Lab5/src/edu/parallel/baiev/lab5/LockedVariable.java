package edu.parallel.baiev.lab5;

public class LockedVariable<T> {
    private final Lock lock;
    private T value;

    public LockedVariable(T startValue) {
        this.lock = new Lock();
        this.value = startValue;
    }

    public T setValue(T value) {
        try {
            this.lock.lock();
            this.value = value;
            this.lock.unlock();
        } catch (InterruptedException exception) {
            System.out.println("Exception occured!");
            exception.printStackTrace();
        }
        return this.value;
    }

    public T getValue() {
        try {
            this.lock.lock();
            return this.value;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } finally {
            this.lock.unlock();
        }
    }
}
