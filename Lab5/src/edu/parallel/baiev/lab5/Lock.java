package edu.parallel.baiev.lab5;

public class Lock {

    private boolean isLocked;

    public boolean isLocked() {
        return isLocked;
    }

    public synchronized void lock() throws InterruptedException {
        if (this.isLocked) {
            wait();
        }
        this.isLocked = true;
    }

    public synchronized void unlock() {
        this.isLocked = false;
        notify();
    }

    public void setLocked(boolean locked) {
        isLocked = locked;
    }
}
