package edu.parallel.baiev.lab5;

public class App {

    public static void main(String[] args) {
        LockedIntVariable intVariable = new LockedIntVariable(3);
        Thread[] threads = new Thread[]{new LabThread("First", intVariable),
                new LabThread("Second", intVariable)
        };
        for (Thread thread : threads) {
            thread.start();
        }
        while (intVariable.getValue() != 13) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException exception) {
                System.out.println("Thread was interrupted");
            }
        }
        System.out.println("Main thread is stopping. Variable: " + intVariable.getValue());
    }
}
