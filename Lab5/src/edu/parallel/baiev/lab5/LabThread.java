package edu.parallel.baiev.lab5;

public class LabThread extends Thread {
    private final String name;
    private final LockedIntVariable variable;

    public LabThread(String name, LockedIntVariable variable) {
        this.name = name;
        this.variable = variable;
    }

    @Override
    public void run() {
        System.out.printf("Thread %s has started\n", name);
        for (int i = 0; i < 5; ++i) {
            variable.inc();
            System.out.printf("Thread %s new value %d\n", name, variable.getValue());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException exception) {
                System.out.println(exception);
            }
        }
    }
}
