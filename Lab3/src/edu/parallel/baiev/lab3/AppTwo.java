package edu.parallel.baiev.lab3;

public class AppTwo {
    public static void main(String[] args) {
        System.out.println("Main process started");
        CommonVariable variableOne = new CommonVariable();
        Thread t1 = new LabThread("First", variableOne);
        Thread t2 = new LabThread("Second", variableOne);
        Thread t3 = new LabThread("Third", variableOne);

        t1.start();
        t2.start();
        t3.start();
        System.out.println("Main process: " + variableOne.state);
        try {
            Thread.sleep(60);
        } catch (InterruptedException ie) {
        }
        variableOne.state = true;
        System.out.println("Main process first true");
        try {
            Thread.sleep(20);
        } catch (InterruptedException ie) {
        }
//        variableTwo.state = true;
        System.out.println("Main process second true");
        System.out.println("Main process ended");
    }
}
