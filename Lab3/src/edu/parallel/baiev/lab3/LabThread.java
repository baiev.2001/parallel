package edu.parallel.baiev.lab3;

public class LabThread extends Thread {
    private final String threadName;
    private final CommonVariable commonVariable;

    public String getThreadName() {
        return threadName;
    }

    public CommonVariable getCommonVariable() {
        return commonVariable;
    }

    public LabThread(String name, CommonVariable variable) {
        threadName = name;
        commonVariable = variable;
        System.out.println(threadName + " - Created");
    }

    public void run() {
        System.out.println(threadName + " - Start of Work");
        short total = 0;
        while (!commonVariable.state) {
            ++total;
//            System.out.println(threadName + " - " + commonVariable.state);
            try {
                Thread.sleep(10);
            } catch (InterruptedException ie) {
            }
        }
        System.out.println(threadName + " - " + commonVariable.state);
        System.out.println(threadName + " - End of Work");
        System.out.println(threadName + " total cycles: " + total);

    }
}
