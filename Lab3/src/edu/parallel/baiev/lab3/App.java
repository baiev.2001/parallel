package edu.parallel.baiev.lab3;

public class App {

    public static void main(String[] args) {
        System.out.println("Main process started");
        CommonVariable cv = new CommonVariable();
        Thread t1 = new LabThread("First", cv);
        Thread t2 = new LongThread("Second", cv);
        Thread t3 = new LabThread("Third", cv);

        t1.start();
        t2.start();
        t3.start();
        System.out.println("Main process: " + cv.state);
        try {
            Thread.sleep(60);
        } catch (InterruptedException ie) {
        }
        cv.state = true;
        System.out.println("Main process: " + cv.state);
        System.out.println("Main process ended");
    }
}
