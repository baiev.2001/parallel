package edu.parallel.baiev.lab3;

public class LongThread extends Thread {

    private final String threadName;
    private final CommonVariable variable;

    public LongThread(String name, CommonVariable variable) {
        this.threadName = name;
        this.variable = variable;
    }

    @Override
    public void run() {
        System.out.println(threadName + " - Start of Work");
        boolean previousRun = false;
        short total = 0;
        while (!variable.state || !previousRun) {
            ++total;
            previousRun = variable.state;
//            System.out.println(threadName + " - " + variable.state);
            try {
                Thread.sleep(10);
            } catch (InterruptedException ie) {
            }
        }
        System.out.println(threadName + " - " + variable.state);
        System.out.println(threadName + " - End of Work");
        System.out.println(threadName + " total cycles: " + total);
    }
}
