package edu.baiev.parallel.lab10;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class App {
    public static void main(String[] args) {
        int highestNumber = 10_000;
        int amountOfTasks = 5;

        Future<Collection<Integer>>[] results = new Future[amountOfTasks];
        ExecutorService firstExecutor = Executors.newFixedThreadPool(amountOfTasks / 2);
        ExecutorService secondExecutor = Executors.newFixedThreadPool(amountOfTasks / 2);
        int numbersPerTask = highestNumber / amountOfTasks;

        IndividualCallableImpl[] individualCallables = new IndividualCallableImpl[amountOfTasks];
        for (int i = 0; i < amountOfTasks; ++i) {
            if (i == 0) {
                individualCallables[i] = new IndividualCallableImpl(0, numbersPerTask * (i + 1));
            } else {
                individualCallables[i] = new IndividualCallableImpl((numbersPerTask * i) + 1, numbersPerTask * (i + 1));
            }
        }

        for (int i = 0; i < amountOfTasks / 2; ++i) {
            results[i] = firstExecutor.submit(individualCallables[i]);
        }
        for (int i = amountOfTasks / 2; i < amountOfTasks; ++i) {
            results[i] = secondExecutor.submit(individualCallables[i]);
        }

        List<Integer> collectedNumbers = Stream.of(results)
                .map(result -> {
                    try {
                        return result.get();
                    } catch (InterruptedException | ExecutionException exception) {
                        throw new RuntimeException(exception);
                    }
                })
                .flatMap(Collection::stream)
                .collect(Collectors.toList());

        for (Integer number : collectedNumbers) {
            System.out.printf("%d, ", number);
        }
        firstExecutor.shutdown();
        secondExecutor.shutdown();
    }
}
