package edu.baiev.parallel.lab10;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.Callable;

public class IndividualCallableImpl implements Callable<Collection<Integer>> {
    private final int lowBound;
    private final int highBound;

    public IndividualCallableImpl(int lowBound, int highBound) {
        this.lowBound = lowBound;
        this.highBound = highBound;
    }

    @Override
    public Collection<Integer> call() {
        Collection<Integer> integers = new ArrayList<>();
        for (int i = lowBound; i <= highBound; ++i) {
            if (condition(i)) {
                integers.add(i);
            }
        }
        return integers;
    }

    private boolean condition(Integer number) {
        return number % 3 == 0
                && number % 5 == 0
                && number % 7 == 0;
//              If number % 3 == 0, then number % 9 == 0 also true
    }
}
