package edu.baiev.parallel.lab10;

import java.util.Random;
import java.util.concurrent.Callable;

public class CallableImpl implements Callable<Integer> {

    private static final Random rand = new Random();
    int threadNumber = 0;

    public void setThreadNumber(int num) {
        threadNumber = num;
    }

    public Integer call() {
        System.out.println(" Callable task (" + threadNumber +
                ") begin ");
        busy();
        System.out.println(" Callable task (" + threadNumber +
                ") end");
        return new Integer(threadNumber);
    }

    private void busy() {
        try {
            Thread.sleep(rand.nextInt(500));
        } catch (InterruptedException e) {
        }
    }
}
