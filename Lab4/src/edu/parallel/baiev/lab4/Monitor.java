package edu.parallel.baiev.lab4;

public class Monitor implements Runnable {
    int procNum = 0;

    Monitor(int procNumber) {
        Thread[] threads = new TestThread[procNumber];
        for (int i = 0; i < procNumber; ++i) {
            threads[i] = new TestThread("Proc:" + i, this);
            threads[i].start();
        }
    }

    public void run() {
        System.out.println("Monitor - Started: " + getProcNum());
        try {
            while (getProcNum() != 0) {
                synchronized (this) {
                    System.out.println("Monitor - Waiting: " + getProcNum());
                    wait();
                    System.out.println("Monitor - Signal received: " + getProcNum());
                    if (getProcNum() == 3) {
                        System.out.println("Viktor Baiev");
                    }
                }
            }
        } catch (InterruptedException ee) {
            System.out.println("Monitor - Interrupted Exception: " + ee);
        }
        System.out.println("Monitor - Ended: " + getProcNum());
    }

    public synchronized void procIncrement() {
        ++procNum;
    }

    public synchronized void procDecrement() {
        --procNum;
    }

    public synchronized int getProcNum() {
        return (procNum);
    }
}
