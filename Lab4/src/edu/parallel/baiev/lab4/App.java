package edu.parallel.baiev.lab4;

public class App {
    public static void main(String argc[]) {
        System.out.println("Main process started");
        Monitor m = new Monitor(5);
        new Thread(m).start();
        System.out.println("Main process ended");
    }
}
