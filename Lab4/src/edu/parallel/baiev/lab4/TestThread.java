package edu.parallel.baiev.lab4;

public class TestThread extends Thread {
    private final String threadName;
    private final Monitor m;

    TestThread(String name, Monitor mm) {
        threadName = name;
        m = mm;
        System.out.println(threadName + " - Created");
    }

    public void run() {
        m.procIncrement();
        System.out.println(threadName + " - Start of Work");
        try {
            Thread.sleep(100);
        } catch (InterruptedException ie) {
        }
        m.procDecrement();
        synchronized (m) {
            m.notify();
            System.out.println(threadName + " - Signal sent");
        }
        System.out.println(threadName + " - End of Work");
    }

}
