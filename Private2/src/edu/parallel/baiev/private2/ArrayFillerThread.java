package edu.parallel.baiev.private2;

import java.util.Arrays;

public class ArrayFillerThread extends Thread {
    private float[] array;
    private ThreadsMonitor monitor;

    public ArrayFillerThread(float[] array, ThreadsMonitor monitor) {
        this.array = array;
        this.monitor = monitor;
    }

    @Override
    public void run() {
        Arrays.fill(array, 1.0f);
        monitor.threadsDec();
    }
}
