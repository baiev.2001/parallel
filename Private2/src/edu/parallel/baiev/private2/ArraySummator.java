package edu.parallel.baiev.private2;

public class ArraySummator extends Thread {
    private float[] arrayA;
    private float[] arrayB;
    private float[] result;
    private ThreadsMonitor monitor;

    public ArraySummator(float[] arrayA, float[] arrayB, float[] result, ThreadsMonitor monitor) {
        this.arrayA = arrayA;
        this.arrayB = arrayB;
        this.result = result;
        this.monitor = monitor;
    }

    @Override
    public void run() {
        if (arrayA.length != arrayB.length
                || arrayA.length != result.length) {
            throw new RuntimeException("Can't calculate the result, because length of arrays");
        }
        for (int i = 0; i < arrayA.length; ++i) {
            result[i] = arrayA[i] + arrayB[i];
        }
        monitor.threadsDec();
    }
}
