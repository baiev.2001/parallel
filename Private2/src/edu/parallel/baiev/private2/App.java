package edu.parallel.baiev.private2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {
    public static void main(String[] args) {
        int n = 4096;
        long l = System.currentTimeMillis();
        int threads = 16;

        float[][] matrixA = new float[n][n];
        float[][] matrixB = new float[n][n];
        float[][] matrixC = new float[n][n];

        ThreadsMonitor monitor = new ThreadsMonitor();
        ExecutorService executorService = Executors.newFixedThreadPool(threads);

        for (int i = 0; i < matrixA.length; ++i) {
            executorService.submit(new ArrayFillerThread(matrixA[i], monitor));
            monitor.threadsInc();
        }
        for (int i = 0; i < matrixB.length; ++i) {
            executorService.submit(new ArrayFillerThread(matrixB[i], monitor));
            monitor.threadsInc();
        }
        monitor.await();
//        printMatrix(matrixA);
//        printMatrix(matrixB);

        for (int i = 0; i < matrixC.length; ++i) {
            executorService.submit(new ArraySummator(matrixA[i], matrixB[i], matrixC[i], monitor));
            monitor.threadsInc();
        }
        monitor.await();
//        printMatrix(matrixC);
        System.out.println("Time: " + (System.currentTimeMillis() - l));

        executorService.shutdown();
    }


    static void printMatrix(float[][] arr) {
        for (int i = 0; i < arr.length; ++i) {
            for (int j = 0; j < arr[i].length; ++j) {
                System.out.print(arr[i][j] + "  ");
            }
            System.out.println();
        }
        System.out.println();
    }
}
