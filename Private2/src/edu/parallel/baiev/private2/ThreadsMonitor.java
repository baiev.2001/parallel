package edu.parallel.baiev.private2;

public class ThreadsMonitor {
    int threads = 0;

    public ThreadsMonitor() {
    }

    public ThreadsMonitor(int threads) {
        this.threads = threads;
    }

    public synchronized void await() {
        try {
            while (this.threads > 0) {
                wait();
            }
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception);
        }
    }

    public synchronized void threadsInc() {
        ++threads;
    }

    public synchronized void threadsDec() {
        --threads;
        this.notify();
    }

    public synchronized int getThreads() {
        return (threads);
    }
}
