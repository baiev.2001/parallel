package edu.chmnu.baiev.lab7;

import java.util.concurrent.CountDownLatch;

public class LabThread extends Thread {
    private boolean[] arrayToFill;
    private CountDownLatch currentLatch;
    private CountDownLatch nextLatch;
    private int startIndex;
    private int endIndex;

    public LabThread(boolean[] arrayToFill,
                       CountDownLatch currentLatch,
                       CountDownLatch nextLatch,
                       int startIndex,
                       int endIndex) {
        this.arrayToFill = arrayToFill;
        this.currentLatch = currentLatch;
        this.nextLatch = nextLatch;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }


    @Override
    public void run() {
        try {
            if (currentLatch != null) {
                currentLatch.await();
            }
//            int highBound = Math.min(arrayToFill.length, startIndex + itemsToProcess);
//            System.out.println("high bound is: " + endIndex);
            for (int i = startIndex; i < endIndex; ++i) {
                arrayToFill[i] = !arrayToFill[i];
            }
            if (nextLatch != null) {
                nextLatch.countDown();
            }
        } catch (InterruptedException exception) {
            throw new RuntimeException(exception);
        }
    }
}
