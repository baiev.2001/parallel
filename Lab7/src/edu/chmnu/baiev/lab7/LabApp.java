package edu.chmnu.baiev.lab7;

import java.util.concurrent.CountDownLatch;

public class LabApp {
    public static void main(String[] args) {
        int threads = 5;
        try {
            threads = Integer.parseInt(args[0]);
        } catch (NumberFormatException exception) {
            System.out.printf("Something wrong with input threads amount. Using default value of %d threads", threads);
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.out.printf("You may configure amount of threads with command line. Using default value of %d threads", threads);
        }
        boolean[] arr = new boolean[16];
        CountDownLatch nextLatch = new CountDownLatch(1);
        CountDownLatch currentLatch = null;
        for (int i = 0; i < threads; ++i) {
            Thread thr;
            if (i >= threads - 1) {
                thr = new LabThread(arr,
                        currentLatch, // Latch to await
                        nextLatch,  // Latch to count down after job is performed
                        (arr.length / threads) * i,
                        arr.length);
            } else {
                thr = new LabThread(arr,
                        currentLatch, // Latch to await
                        nextLatch,  // Latch to count down after job is performed
                        (arr.length / threads) * i,
                        (arr.length / threads) * (i + 1));
            }

            currentLatch = nextLatch;
            nextLatch = new CountDownLatch(1);
            thr.start();
        }
        try {
            currentLatch.await();
        } catch (InterruptedException exception) {
            System.out.println("ERRROR. EXCEPTION OCCURED");
            exception.printStackTrace();
        }
        for (int i = 0; i < arr.length; ++i) {
            if (!arr[i]) {
                throw new RuntimeException("Bug in program. Some value had false value. Id: " + i);
            }
//            System.out.print(arr[i] + " ");
        }
    }
}
