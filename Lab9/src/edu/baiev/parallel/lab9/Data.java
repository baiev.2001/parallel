package edu.baiev.parallel.lab9;

public class Data {
    private int id;

    public Data() {
    }

    public Data(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
