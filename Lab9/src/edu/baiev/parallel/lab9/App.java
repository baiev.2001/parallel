package edu.baiev.parallel.lab9;

import java.util.concurrent.Exchanger;

public class App {

    public static void main(String[] args) {
        Exchanger<Data> mainOneExchanger = new Exchanger<>();
        Exchanger<Data> oneTwoExchanger = new Exchanger<>();
        Exchanger<Data> twoThreeExchanger = new Exchanger<>();

        LabThread threadOne = new LabThread(mainOneExchanger, oneTwoExchanger);
        LabThread threadTwo = new LabThread(oneTwoExchanger, twoThreeExchanger);
        LabThread threadThree = new LabThread(twoThreeExchanger, null);
        new Thread(threadOne).start();
        new Thread(threadTwo).start();
        new Thread(threadThree).start();

        try {
            mainOneExchanger.exchange(new Data(1));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
