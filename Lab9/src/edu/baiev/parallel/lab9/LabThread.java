package edu.baiev.parallel.lab9;

import java.util.concurrent.Exchanger;

public class LabThread implements Runnable {
    private final Exchanger<Data> receiveQueue;
    private final Exchanger<Data> sendQueue;

    public LabThread(Exchanger<Data> receiveQueue, Exchanger<Data> sendQueue) {
        this.receiveQueue = receiveQueue;
        this.sendQueue = sendQueue;
    }

    @Override
    public void run() {
        try {
            Data data = receiveQueue.exchange(null);
            System.out.printf("Received data: %d\n", data.getId());
            if (sendQueue != null) {
                sendQueue.exchange(data);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
